<?php

require_once __DIR__.'/../vendor/autoload.php';

Predis\Autoloader::register();

try {
        $redis = new Predis\Client(array(
            "scheme" => "tcp",
            "host" => "redis",
            "port" => 6379
        ));
}
catch (Exception $e) {
    die($e->getMessage());
}
