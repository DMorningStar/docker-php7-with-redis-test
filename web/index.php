<?php
include 'redis-conf.php';

if(!$redis->exists('counter')) {
    $redis->set('counter', 0);
}

$redis->incr('counter');

echo $redis->get('counter');
